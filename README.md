### What is this?

This is just a dummy laravel installation that you can perform in order to practive your setup process.
Make sure to use homestead (https://laravel.com/docs/5.5/homestead#per-project-installation)
Run `vagrant up` and make sure you are using box 'laravel/homestead' (v4.0.0), that way you don't have to download the box in class (eduroam is slow)  